(function () {

    angular
        .module('app')
        .service('Field', field);

    function field() {

        var service = {
            id: null,
            name: null,
            type: "text",
            value: false
        };

        return _constructor;

        /////////////////

        function _constructor(id, name) {
            this.id = id;
            this.name = name;
            this.type = "text";
            this.value = "";
        }
    }
})();