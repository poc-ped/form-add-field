(function () {

    angular
        .module('app')
        .service('Form', form);

    form.$inject = ['Field'];

    function form(Field) {

        var service = {
            lastId: 0,
            fields: [],
            add: add,
            remove: remove
        };

        return function() {
            return {
                lastId: 0,
                fields: [],
                add: add,
                remove: remove
            };
        }

        /////////////////

        function add() {
            var id = this.lastId++;
            var field = new Field(id, "ok");

            this.fields.push(field);
        }

        function remove(id) {
            this.fields.splice(id, 1);
        }
    }
})();