(function () {

    angular
        .module('app')
        .controller('FormController', form);

    form.$inject = ['$scope', 'Form'];

    function form($scope, Form) {
        $scope.form = new Form();
    }
})();