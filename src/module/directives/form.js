(function () {

    angular
        .module('app')
        .directive('renanForm', renanForm);

    renanForm.$inject = ['Form'];

    function renanForm(Form) {

        var directive = {
            restrict: 'EA',
            transclude: true,
            templateUrl: 'module/views/form.html',
            controller: 'FormController',
            scope: {
                name: '@'
            }
        };

        return directive;
    }
})();